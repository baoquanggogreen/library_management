<?php 
	include 'model/model.php';
	class Controller {
		function handleRequest(){
			$action = isset($_GET['action'])?$_GET['action']:'home';
			$model = new Model();
			switch ($action) {
				case 'home':
					// lay thong tin trang chu ra
					$homeData = $model->getHomepage();
					break;
				case 'about':
					// lay thong tin trang about ra
					$aboutData = $model->getAbout();
					break;
				case 'comic':
					$comicData = $model->getComic();
					include 'view/comic.php';
					break;
				case 'delete_comic':
					// lay id can xoa
					$idDelete = $_GET['ID_Comic'];
					// lay thong tin trang news ra
					if ($model->deleteComic($idDelete) === TRUE) {
						header('Location: index.php?action=comic');
					}
					break;
				case 'add_comic':
					$errT = '';
					if (isset($_POST['add_comic'])) {
						$Title = $_POST['Title'];
						$Borrow_Time = $_POST['Borrow_Time'];
						$Return_Time = $_POST['Return_Time'];
						$Name = $_POST['Name'];
						if(empty($Title)||empty($Borrow_Time)||empty($Name)) {
							$errT = "Chưa nhập đủ thông tin!";
						}
						// xu ly luu news vao database
						else if ($model->addComic($Title, $Borrow_Time, $Return_Time, $Name) === TRUE) {
							header('Location: index.php?action=comic');
						}
					}
					include 'view/add.php';
					break;
				case 'edit_comic':
					$ID_Comic = $_GET['ID_Comic'];
					$oldData = $model->getDetailId($ID_Comic);
					$errD = '';
					if (isset($_POST['edit_comic'])) {
						$Title = $_POST['Title'];
                        $Borrow_Time = $_POST['Borrow_Time'];
                        $Return_Time = $_POST['Return_Time'];
						$Name = $_POST['Name'];
						if(empty($Title)||empty($Borrow_Time)||empty($Name)) {
							$errD = "Chưa nhập đủ thông tin!";
						}
						// xu ly luu info vao database
						else if ($model->editComic($ID_Comic, $Title, $Borrow_Time, $Return_Time, $Name) === TRUE) {
							header('Location: index.php?action=comic');
						}
					}
					include 'view/update.php';
					break;
				case 'register':
					if (isset($_POST['register'])) {
						$username = $_POST['username'];
						$password = $_POST['password'];
						if ($model->addUsers($username, $password) === TRUE) {
							header('Location: index.php?action=index');
						}
					}
					# code...
					include 'view/register.php';
					break;	
				case 'login':
					$err = '';
					if (isset($_POST['login'])) {
						$username = $_POST['username'];
						$password = $_POST['password'];
						// xu ly luu users vao database
						$login = $model->login($username, $password);
						$login = $login->fetch_assoc();
						if (!is_null($login)) {
							$_SESSION['username'] = $login['username']; 
							header('Location: index.php?action=index');
						} else {
							$err = 'Sai username hoac password';
						}
					}
					include 'view/login.php';
					break;
				case 'logout':
							unset($_SESSION['username']);
							header('Location: index.php?action=index');
							break;		
				default:
					# code...
					break;
			}
		}
	}
?>