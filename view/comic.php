<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>ADD COMIC</title>
</head>

<body>
    <table class="table">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Title</th>
            <th scope="col">Borrow_Time</th>
            <th scope="col">Return_Time</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
        </tr>
        <?php 
			while ($row = $comicData->fetch_assoc()) {
				$ID_Comic = $row['ID_Comic'];
				echo "<tr>";
				echo "<td>".$row['ID_Comic']."</td>";
				echo "<td>".$row['Title']."</td>";
				echo "<td>".$row['Borrow_Time']."</td>";
				echo "<td>".$row['Return_Time']."</td>";
                echo "<td>".$row['Name']."</td>";
                echo "<td><a href='index.php?action=delete_comic&ID_Comic=".$ID_Comic."''>Delete</a> 
                                | <a href='index.php?action=edit_comic&ID_Comic=".$ID_Comic."''>Edit</a>
                        </td>";
                echo "</tr>";
            }?>
    </table>
    <div class="text-center">
        <a type='button' class='btn btn-success' href='index.php?action=add_comic'>Add Comic</a>
    </div>
</body>

</html>