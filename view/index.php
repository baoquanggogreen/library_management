<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
    <title>Comic Library</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>
    <h2 class="text-center text-secondary">Comic Manager</h2>
    <div class="navbar navbar-expand-sm bg-light">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="index.php?action=home">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?action=about">About</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?action=comic">Comic</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?action=contact">Contact</a></li>
            <?php if (!isset($_SESSION['username'])) {?>
            <li class="nav-item"><a class="nav-link" href="index.php?action=register">Register</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?action=login">Login</a></li>
            <?php } ?>
            <?php if(isset($_SESSION['username'])) { ?>
            <li class="nav-item"><a class="nav-link" href="index.php?action=logout">Logout</li>
            <li class="nav-item text-danger">Welcome <?php echo $_SESSION['username']?></li>
            <?php }?>
        </ul>
    </div>
    <?php 
		include 'controller/controller.php';
		$controller = new Controller;
		$controller->handleRequest();
	?>
</body>

</html>