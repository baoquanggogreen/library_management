# library_management
This is the library management project as well as a project in school subject.
In this project, I use all PHP. 
These are the basic functions in this project:
- Login, register as ADMIN or USER.
  + ADMIN has right to view data and perform operations like CRUD.
  + USER has only right to view data.
- Add, edit, delete data in main table.
Thanks for reading!